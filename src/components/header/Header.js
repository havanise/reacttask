import React, {useState} from 'react';
import "./header.css"
import Main from "../main/Main";
import Footer from "../footer/Footer";
import Plan from "../plan/Plan"
import Preference from "../preference/Preference"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
} from "react-router-dom";

const Header = (props) => {
    const [buttonText, setButtonText] = useState("Next");

    return (
        <div className="white">
            <div className="header">
                <p>STEP {props.numberPage === 1 ? 1 :
                    props.numberPage===2? 2:
                        3
                } OF 3</p>
                <p>Lost or have trouble? <a href="#" className="before_arrow">Get help</a></p>
            </div>
            {props.numberPage===1? <div><Main/>
                    <Footer numberPage={props.numberPage} setPagenum={props.setPagenum}/></div>:
                props.numberPage===2? <div>
                    <Plan/><Footer numberPage={props.numberPage} setPagenum={props.setPagenum}/>
                </div>:
                    <div><Preference/><Footer numberPage={props.numberPage} setPagenum={props.setPagenum}/></div>}
        </div>

    )
}
export default  Header;