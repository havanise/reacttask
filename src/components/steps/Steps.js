import React from 'react';
import "./steps.css"

const Steps = () =>{
    return (
        <div className="progresses">
            <div className="progress"><span className="square square_after">&#9744;</span><p>Contact details</p></div>
            <div className="progress"><span className="square square_after">&#9744;</span><p>Investment plans</p></div>
            <div className="progress"><span className="square">&#9744;</span><p>Investment preferences</p></div>
        </div>
    )
}
export default  Steps;