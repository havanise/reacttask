import React from 'react';
import "./blue.css"
import Note from "../note/Note";
import Steps from "../steps/Steps";
const Blue = (props) => {
    return (
        <div className="blue">
            <h2 className="logo">united <span className="opacity">properties</span></h2>
            <Steps/>
            <Note numberPage={props.numberPage}/>
        </div>
    )
}
export default  Blue;