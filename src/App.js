import React, {useState} from 'react';
import './App.css';
import Blue from "./components/blue/Blue";
import Header from "./components/header/Header";

const App = () => {
    let [page, setPage] = useState(1);
  return (
        <div className="App">
                    <Blue numberPage={page}/>
                    <Header numberPage={page} setPagenum={setPage}/>
        </div>
  );
}

export default App;
