import React from "react";
import "./preference.css"
const Preference = () => {
    return (
        <div className="main">
            <h2>Investment preferences</h2>
            <p>This will help us figure out what your investment options are so that we can show you only possibly
                intresting for you deals</p>
            <h4>What kind of real estate are you interested in?</h4>
            <div className="checkbox">
                <div className="check">
                    <input type="checkbox" id="fam" />
                    <label className="container" htmlFor="fam" id="yes">Single family</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="multifam"/>
                    <label className="container" htmlFor="multifam">Residential multifamily</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="retail"/>
                    <label className="container " htmlFor="retail">Commercial retail</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="industrial"/>
                    <label className="container" htmlFor="industrial">Commercial industrial</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="hospitality"/>
                    <label className="container" htmlFor="hospitality">Commercial hospitality</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="warehousing"/>
                    <label className="container" htmlFor="warehousing">Commercial warehousing</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="office"/>
                    <label className="container" htmlFor="office">Commercial office</label>
                </div>
                <div className="check">
                    <input type="checkbox" id="Other"/>
                    <label className="container" htmlFor="Other">Other</label>
                </div>
            </div>
        </div>
    )
}
export default Preference;