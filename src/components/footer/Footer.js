import React, {useState} from 'react';
import {Link, useRouteMatch} from "react-router-dom";
import "./footer.css"


const Footer = (props) => {


    return (

        <div className="footer">
            <a href="" className="back_link">Back to the homepage</a>
            <form action="">
                <input type="submit" value="skip for now" className="skip_btn"/>
                <button type="button" className="page_btn" onClick={()=>{props.setPagenum(props.numberPage + 1)}} >
                    {props.numberPage===1?
                    "Next step":
                        props.numberPage===2?
                    "Next step":
                    "Finish"}</button>
            </form>
        </div>
    )
}
export default  Footer;