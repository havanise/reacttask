import React from 'react';
import "./main.css"

const Main = () => {
    return (
        <div className="main">
            <h2>Сontact details</h2>
            <p>Welcome to United Properties, we are glad to see you!
                Let’s get started by letting us know a little bit about you</p>
            <form action="" className="contact_form">
                <div className="input_group">
                    <label htmlFor="fname">First Name</label>
                    <input type="text" id="fname" className="contact_input"/>
                </div>
                <div className="input_group">
                    <label htmlFor="email">E-mail address</label>
                    <input type="email" id="email" className="contact_input"/>
                </div>
                <div className="input_group">
                <label htmlFor="country">Country</label>
                    <select id="country" name="country" className="contact_input">
                        <option value="ukraine">Ukraine</option>
                        <option value="canada">Canada</option>
                        <option value="usa">USA</option>
                    </select>
                </div>
                <h4>Privacy policy</h4>
                <p>
                    We know you care about how your personal information is used and shared, so we take your privacy seriously
                </p>
                <a className="before_arrow" href="#">Expand privacy policy</a>

            </form>
        </div>
    )
}
export default  Main;