import React from "react";
import "./plan.css"
const Plan = () => {
    return (
        <div className="main">
            <h2>Investment plans</h2>
            <p>Let us know about your investment plans.
                This will help us get you to the right expert who will help you further</p>
            <form action="" className="contact_form">
                <h4>How much are you planning to invest in this year?</h4>
                <div className="money">
                    <div className="input_group">
                        <label htmlFor="from">From</label>
                        <input type="text" id="from" className="contact_input"/>
                    </div>
                    <div className="input_group">
                        <label htmlFor="to"> To</label>
                        <input type="text" id="to" className="contact_input"/>
                    </div>
                </div>
                <h4>Are you an accredited investor?</h4>
                <div className="radios">
                    <div className="radio">
                        <input type="radio" id="yes"  name="radio"/>
                        <label className="container" htmlFor="yes">Yes</label>
                    </div>
                    <div className="radio">
                        <input type="radio" id="no" name="radio"/>
                        <label className="container" htmlFor="no">No</label>
                    </div>
                </div>
            </form>
        </div>
    )
}
export default Plan;