import React from 'react';
import "./note.css"

const Note = (props) => {
    return (
        <div className="notes">
            {props.numberPage===1? <div className="note">
                <p>We care about your time, that's why we created a 3-stage onboarding that will not take more than
                    5 minutes to complete</p>
                <div className="note_footer">
                    <div>
                        <p>William Mac</p>
                        <p>CO-FOUNDER, INVESTOR</p>
                    </div>
                    <p>UP</p>
                </div></div>:
                props.numberPage===2? <div className="note">
                <p>Save from thousands to millions on your deal. Secure the best possible.
                    And get independent, unbiased advice for free</p>
                <div className="note_footer">
                    <div>
                        <p>William Mac</p>
                        <p>CO-FOUNDER, INVESTOR</p>
                    </div>
                    <p>UP</p>
                </div></div>:
                <div className="note">
                    <p>United Properties is about fast & easy searching, buying,
                        selling and investing ever — online, with an expert by our side</p>
                    <div className="note_footer">
                        <div>
                            <p>William Mac</p>
                            <p>CO-FOUNDER, INVESTOR</p>
                        </div>
                        <p>UP</p>
                    </div></div>
                    }
        </div>

)
}
export default  Note;